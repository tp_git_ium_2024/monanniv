# TP1: J'organise mon anniversaire



## 1-Créer un depot git "MonAnniv" et le cloner :

### Création:

![Q1](Screenshots/TP1 Q1.png)

### Clonage:

![Q12](Screenshots/TP1 clone.png)


## 2- Créer un fichier pour lister les invites (un par ligne):

![Q2](Screenshots/TP1 Q2.png)

## 3- Créer un fichier pour les idees de cadeaux (un par ligne)

![Q3](Screenshots/TP1 Q3.png)

## 4- Mise a jour des 2 fichiers dans un meme commit

![Q4](Screenshots/TP1 Q4.png)